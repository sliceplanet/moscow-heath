$(window).on('load', function () {
    setTimeout(function() {
        var tag = document.createElement('script');
    
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }, 1500);
    
});

$(function () {
    /*program-item toggle*/
    if($('.program-items').length){
        var expandItem = $('.program-items').attr('data-text-expand');
        var collapseItem = $('.program-items').attr('data-text-collapse');
        $('.program-item__show-text').text(expandItem);
        $('.program-item__show').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
    
            if ($(this).text() == expandItem) {
                // $('.program-item__show-text').text('Развернуть');
                $(this).find('.program-item__show-text').text(collapseItem);
    
            } else {
                $(this).find('.program-item__show-text').text(expandItem);
            }
            // $(this).parent().siblings().find('.program-item__speakers').slideUp();
            $(this).parent().find('.program-item__speakers').slideToggle();
            return false;
        });
    }
    
    /*program-item toggle*/

    /*filter load*/
    if ($('.program-filters__theme').length) {
        $(".program-filters__theme li").slice(0, 5).css('display', 'flex');
        $(".program-filters__more").on('click', function (e) {
            e.preventDefault();
            $(".program-filters__theme li:hidden").slice(0, $('.program-filters__theme li').length).css('display', 'flex');
        });
    }
    /*filter load*/

    /*filter iso*/

    Isotope.Item.prototype._create = function () {
        // assign id, used for original-order sorting
        this.id = this.layout.itemGUID++;
        // transition objects
        this._transn = {
            ingProperties: {},
            clean: {},
            onEnd: {}
        };
        this.sortData = {};
    };

    Isotope.Item.prototype.layoutPosition = function () {
        this.emitEvent('layout', [this]);
    };

    Isotope.prototype.arrange = function (opts) {
        // set any options pass
        this.option(opts);
        this._getIsInstant();
        // just filter
        this.filteredItems = this._filter(this.items);
        // flag for initalized
        this._isLayoutInited = true;
    };

    // layout mode that does not position items
    Isotope.LayoutMode.create('none');

    var isoOptions = {
        itemSelector: '.program-item',
        layoutMode: 'none'
    };

    // init isotope
    var $grid = $('.program-items__grid').isotope(isoOptions);
    var isActive = true;

    $('.program-items__grid').each(function () {
        var $module = $(this);
        var update = (function () {
            $module.isotope('layout');
            //return true;
        });

        this.addEventListener('load', update, true);
    });

    // $('.filter-btn').on('click', function () {
    //     $grid.isotope(isoOptions); // re-initialize
    // });

    // $('.program-filters__refresh').click(function (e) {
    //     e.preventDefault();
    //     $('.filter-btn').removeClass('active');
    //     $('.filter-btn-all').addClass('active');
    //     $grid.isotope('destroy');
    // })


    // var filters = {}



    // $('.program-filters').on('click', '.filter-btn', function (event) {
    //     var $button = $(event.currentTarget);
    //     // get group key
    //     var $buttonGroup = $button.parents('ul');
    //     var filterGroup = $buttonGroup.attr('data-filter-group');
    //     // set filter for group
    //     filters[filterGroup] = $button.attr('data-filter');
    //     // combine filters
    //     var filterValue = concatValues(filters);
    //     // set filter for Isotope
    //     $grid.isotope({ filter: filterValue });
    // });

    // flatten object by concatting values
    // function concatValues(obj) {
    //     var value = '';
    //     for (var prop in obj) {
    //         value += obj[prop];
    //     }
    //     return value;
    // }

    // $('.program-filters__filter').each(function (i, buttonGroup) {
    //     var $buttonGroup = $(buttonGroup);
    //     $buttonGroup.on('click', '.filter-btn-date', function () {
    //         $buttonGroup.find('.filter-btn-date').addClass('inactive');
    //         $buttonGroup.find('.filter-btn-date.active').removeClass('active');
    //         $(this).addClass('active').removeClass('inactive');
    //     });
    // });

    // $('.program-filters__theme').each(function (i, buttonGroup) {
    //     // var $buttonGroup = $(buttonGroup);
    //     // $buttonGroup.on('click', '.filter-btn-theme', function () {
    //     //     $buttonGroup.find('.filter-btn-all').removeClass('active');
    //     //     $(this).toggleClass('active');
    //     //     if( !($('.program-filters__theme').find('.filter-btn-theme.active').length)) {
    //     //         $buttonGroup.find('.filter-btn-all').addClass('active');
    //     //     }
    //     // });
    //     // $buttonGroup.on('click', '.filter-btn-all', function () {
    //     //     $buttonGroup.find('.filter-btn-theme').removeClass('active');
    //     //     $(this).addClass('active');
    //     // });
    //     var $buttonGroup = $(buttonGroup);
    //     $buttonGroup.on('click', '.filter-btn-theme', function () {
    //         $buttonGroup.find('.filter-btn-theme').addClass('inactive');
    //         $buttonGroup.find('.filter-btn-theme.active').removeClass('active');
    //         $(this).addClass('active').removeClass('inactive');
    //     });
    // });

    /*filter iso*/

    var $container = $('.program-items__grid').isotope({
        itemSelector: 'program-item'
    });



    // filter with selects and checkboxes
    var $selects = $('.date-row .filter-btn');
    var $checkboxes = $('.program-filters__theme li');

    $selects.click(function () {
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
    });
    $checkboxes.click(function () {
        // $('.program-filters__theme li').removeClass('active');
        $(this).parent().find('.filter-btn-all').removeClass('active');
        $(this).toggleClass('active');
        if (!($('.program-filters__theme').find('.filter-btn-theme.active').length)) {
            $(this).parent().find('.filter-btn-all').addClass('active');
        }
    });

    $('.program-filters__theme li.filter-btn-all').click(function () {
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
    });

    $selects.add($checkboxes).click(function () {
        // map input values to an array
        var exclusives = [];
        var inclusives = [];
        // exclusive filters from selects
        $selects.each(function (i, elem) {

            if ($(elem).hasClass('active')) {
                exclusives.push($(elem).attr('data-filter'));
            }
        });
        // inclusive filters from checkboxes
        $checkboxes.each(function (i, elem) {
            // if checkbox, use value if checked
            if ($(elem).hasClass('active')) {
                inclusives.push($(elem).attr('data-filter'));
            }
        });

        // combine exclusive and inclusive filters

        // first combine exclusives
        exclusives = exclusives.join('');

        var filterValue;
        if (inclusives.length) {
            // map inclusives with exclusives for
            filterValue = $.map(inclusives, function (value) {
                return value + exclusives;
            });
            filterValue = filterValue.join(', ');
        } else {
            filterValue = exclusives;
        }

        
        $container.isotope({ filter: filterValue })
    });
    $('.program-filters__refresh').click(function (e) {
        e.preventDefault();
        $('.filter-btn').removeClass('active');
        $('.filter-btn-all').addClass('active');
        $container.isotope({ filter: '*' })
    });

    /* play video */

    

    /* play video end */

    let videoSrc;

    $('.btn-live').click(function() {
        videoSrc = $(this).parent().parent().parent().attr('data-video');
        console.log(videoSrc)
        $('.popup-video__overlay').css('display', 'flex');
        $('#popup-video__iframe iframe').attr('src', videoSrc);
    });

    if ($('.popup-video').length) {
        $('.popup-video__overlay-icon').click(function () {
            $(this).parent().css('display', 'none');
        });
    }

    
});

