$(window).on('load', function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('body').addClass('ios');
    } else {
        $('body').addClass('web');
    };
    $('body').removeClass('loaded');

    $(".slick-arrow").mouseover(function () {
        $(".custom-cursor").addClass("custom-cursor_link");
    });
    $(".slick-arrow").mouseout(function () {
        $(".custom-cursor").removeClass("custom-cursor_link");
    });
    jQuery(document).ready(function ($) {
        var myHash = location.hash; //получаем значение хеша
        location.hash = ''; //очищаем хеш
        if (myHash[1] != undefined) { //проверяем, есть ли в хеше какое-то значение
            $('html, body').animate(
                { scrollTop: $(myHash).offset().top }
                , 700); //скроллим за полсекунды
            location.hash = myHash; //возвращаем хеш
        };
    });
    mainBlockAnimation();
    setTimeout(function() {
        new WOW().init();
	}, 1500);
    
});

if($('.js-img').length) {
    var jsImg = $(".js-img");
    new LazyLoad(jsImg, {
        root: null,
        rootMargin: "0px",
        threshold: 0
    });
}	

/* viewport width */
function viewport() {
    var e = window,
        a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {
        width: e[a + 'Width'],
        height: e[a + 'Height']
    }
};
/* viewport width */
$(function () {
    /* placeholder*/
    $('input, textarea').each(function () {
        var placeholder = $(this).attr('placeholder');
        $(this).focus(function () {
            $(this).attr('placeholder', '');
        });
        $(this).focusout(function () {
            $(this).attr('placeholder', placeholder);
        });
    });
    /* placeholder*/

    if ($('.styled').length) {
		$('.styled').styler({
            selectPlaceholder: '',
            filePlaceholder: 'Фотография'
        });
	};

    $('.js-scr').click(function () {
		var target = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 1000);
		return false;
	});
    $('.form-control').focus(function () {
        $(this).parents('.box-field').addClass('focused-field');
    });
    $('.form-control').focusout(function () {
        var val_field = $(this).val().length;
        if (val_field >= 1) {
            $(this).parents('.box-field').addClass('focused-field');
        } else {
            $(this).parents('.box-field').removeClass('focused-field');
        };
    });
    /*button open main nav begin*/
    $('.js-button-nav').click(function () {
        $(this).toggleClass('active');
        $('.main-nav').toggleClass('show-nav');
        $('body').toggleClass('no-scroll');
        $('html').toggleClass('no-scroll');
        return false;
    });
    /*button open main nav end*/
    /*faq accardion begin*/
    $('.list-faq__link').click(function () {
        $(this).parent().siblings().removeClass('active');
        $(this).parent().siblings().find('.list-faq__answer').slideUp();
        $(this).parent().toggleClass('active');
        $(this).parent().find('.list-faq__answer').slideToggle();
        return false;
    });
    /*faq accardion end*/

    /* components begin*/

    if ($('.js-main-slider').length) {
        $('.js-main-slider').slick({
            dots: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right"></i></button>',
            responsive: [{
                breakpoint: 767,
                settings: {
                    arrows: false
                }
            }]
        });

        // $('.js-main-slider').on('afterChange', function(event, slick, currentSlide) {
        //     if(slick.$slides.length -1 == currentSlide) {
        //         $('.main-screen').addClass('speaker-slide');
        //         $('.main-slider__speaker-imgs').addClass('active');
        //     } else {
        //         $('.main-screen').removeClass('speaker-slide');
        //         $('.main-slider__speaker-imgs').removeClass('active');
        //     }
        //     // else if (slick.$slides.length -2 == currentSlide) {
        //     //     $('.main-screen').addClass('speaker-slide');
        //     //     $('.main-slider__speaker-imgs').removeClass('active');
        //     //     $('.main-slider__speaker-imgs2').addClass('active');
        //     // } 
        // });
    };
    if ($('.js-exposition-img').length) {
        $('.js-exposition-img').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right"></i></button>',
            responsive: [{
                breakpoint: 767,
                settings: {
                    arrows: false
                }
            }]
        });
    };
    
    if ($('.js-scroll').length) {
        var $frame = $('.js-scroll');
        var $wrap = $frame.parent();

        // Call Sly on frame
        $frame.sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateMiddle: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 3,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            cycleBy: 'items',
            cycleInterval: 1500,  // Cycle interval
            pauseOnHover: true,
            startPaused:   false,


            // Buttons
            prev: $wrap.find('.prev'),
            next: $wrap.find('.next')
        });
        $(window).on('resize', function () { $frame.sly('reload'); });
    };
    if ($('.js-fancybox').length) {
        $('.js-fancybox').fancybox({
            autoDimensions: true,
            margin: 5,
            padding: 0,
            afterClose: function(instance) {
                if (instance.href === '#video-popup') {
                    $('#popup-video__iframe iframe').attr('src', '');
                }
        }
        });
    };

    $(document).on('click', '.js-fancybox-close', function () {
        $('.fancybox-item.fancybox-close').click();
        return false;
    });

    $('.js-speaker-ajax').click(function(event) {
        event.preventDefault();
        const element = event.delegateTarget;
        const url = element.href;
        const popup = element.dataset.popup;
        const selector = '#' + popup;
        if ($(selector).length) {
            $.fancybox.open($(selector));
        } else {
            $.ajax({
                method: 'GET',
                url: url,
                contentType: 'html',
                success: function (data) {
                    $('#response').append(data);
                    $(selector + ' .js-img').lazyload();
                    $.fancybox.open($(selector));
                },
                error: function () {
                    $.fancybox.open($('#error-popup'));
                },
                fail: function () {
                    $.fancybox.open($('#error-popup'));
                },
            });
        }
    });
    if ($('.validate-form-js').length) {
        $('.validate-form-js').each(function () {
            $(this).validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 1
                    },
                    email: {
                        required: true,
                        minlength: 1,

                    },
                    question: {
                        required: true,
                        minlength: 1
                    },
                    company: {
                        required: true,
                        minlength: 1
                    }
                },
                messages: {
                    name: {
                        required: "Некорректные данные. Попробуйте еще раз.",

                    },
                    email: {
                        required: "Некорректные данные. Попробуйте еще раз.",
                        email: "Некорректно заполнен e-mail"
                    },
                    question: {
                        required: "Некорректные данные. Попробуйте еще раз.",

                    },
                    company: {
                        required: "Некорректные данные. Попробуйте еще раз."
                    }
                },

                submitHandler: function (form) {
                    callAjaxForm(form);
                }
            });
        })
    }

    if ($('.validate-form-js2').length) {
        $('.validate-form-js2').each(function () {
            $(this).validate({
                rules: {
                    surname: {
                        required: true,
                        minlength: 1
                    },
                    name: {
                        required: true,
                        minlength: 1
                    },
                    date: {
                        required: true,
                        dateISO: true
                    },
                    phone: {
                        required: true,
                        number: true
                    },
                    mail: {
                        required: true,
                        email: true
                    },
                    extraSurname: {
                        required: true,
                        minlength: 1
                    },
                    extraName: {
                        required: true,
                        minlength: 1
                    },
                    extraPhone: {
                        required: true,
                        number: true
                    },
                    sex: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                    password2: {
                        required: true,
                        equalTo: "#pass"
                    }
                },
                messages: {
                    surname: {
                        required: "Некорректные данные. Попробуйте еще раз."

                    },
                    name: {
                        required: "Некорректные данные. Попробуйте еще раз."
                    },
                    date: {
                        required: "Некорректные данные. Попробуйте еще раз.",
                        dateISO: "Некорректные данные. Попробуйте еще раз."
                    },
                    phone: {
                        required: "Некорректные данные. Попробуйте еще раз.",
                        number: "Некорректные данные. Попробуйте еще раз."
                    },
                    mail: {
                        required: "Некорректные данные. Попробуйте еще раз.",
                        email: "Некорректные данные. Попробуйте еще раз."
                    },
                    extraSurname: {
                        required: "Некорректные данные. Попробуйте еще раз."
                    },
                    extraName: {
                        required: "Некорректные данные. Попробуйте еще раз."
                    },
                    extraPhone: {
                        required: "Некорректные данные. Попробуйте еще раз.",
                        number: "Некорректные данные. Попробуйте еще раз."
                    },
                    sex: {
                        required: "Некорректные данные. Попробуйте еще раз."
                    },
                    password: {
                        required: "Некорректные данные. Попробуйте еще раз."
                    },
                    password2: {
                        required: "Некорректные данные. Попробуйте еще раз.",
                        equalTo: "Некорректные данные. Попробуйте еще раз."
                    }
                },

                submitHandler: function (form) {
                    callAjaxFormRace(form);
                }
            });
        })
    }

    function callAjaxForm(form) {
        var jform = $(form);
        var statusUrl = jform.attr('action');
        $.ajax({
            url: statusUrl,
            data: jform.serialize(),
            success: function () {
                $.fancybox.open($('#success-popup'));
                form.reset();
            },
            fail: function () {
                $.fancybox.open($('#error-popup'));
            },
            error: function () {
                $.fancybox.open($('#error-popup'));
            }
        });
    }

    function callAjaxFormRace(form) {
        var jform = $(form);
        var statusUrl = jform.attr('action');
        var formData = new FormData(form);
        $.ajax({
            method: 'POST',
            url: statusUrl,
            data: formData,
            processData: false,
            contentType: false,
            success: function () {
                $.fancybox.open($('#success-popup'));
                form.reset();
            },
            fail: function () {
                $.fancybox.open($('#error-popup'));
            },
            error: function () {
                $.fancybox.open($('#error-popup'));
            }
        });
    }

    /* components end*/

    /* speakers load */

    // if ($('.speakers-screen__items').length) {
    //     $(".speakers-screen__item").slice(0, 9).css('display', 'block');
    //     $(".speakers-screen__more").on('click', function (e) {
    //         e.preventDefault();
    //         $(".speakers-screen__item:hidden").slice(0, 3).css('display', 'block');
    //     });
    // }

    /* speakers load end */

    $('.popup .program-item__text .btn').click(function (e) {
        $.fancybox.close();
    });


    if ($('.js-exhibition-img').length) {
        $('.js-exhibition-img').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><i class="icon-arrow-left"></i></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><i class="icon-arrow-right"></i></button>',
            responsive: [{
                breakpoint: 767,
                settings: {
                    arrows: false
                }
            }]
        });
    };

    if ($('.speakers-list__name').length) {
        $('.speakers-list__name').hover(function() {
            $(this).parent().find('.speakers-list__img').toggleClass('hover');
        });
    };

    if($('.box-lang').length) {
        $('.list-lang__item').click(function(){
            $('.list-lang__item').removeClass('active');
            $(this).addClass('active');
        });
    }

    $(".js-top").click(function () {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
    });
    

    if ($('.main-slider__video').length) {
        $('.main-slider__video__overlay-icon').click(function () {
            $(this).parent().css('display', 'none');
        });
    };

    if ($('.gallery-imgs__slider').length) {
		$('.gallery-main-img__slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
            fade: true,
			asNavFor: '.gallery-imgs__slider'
        });
        $('.gallery-imgs__slider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
            var dataId = $('.slick-current').attr("data-slick-index");    
            $('.gallery-imgs__nav-current').text(+dataId + 1);
            $('.gallery-imgs__nav-count').text(slick.slideCount);
        });
		$('.gallery-imgs__slider').slick({
			prevArrow: $('.arrowLeft'),
			nextArrow: $('.arrowRight'),
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: '.gallery-main-img__slider',
            focusOnSelect: true,
			responsive: [
				{
					breakpoint: 1240,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 1
					}
				}
            ],
        });
        
	};

});




var handler = function () {
    var height_footer = $('footer').height();
    var height_header = $('header').height();
    //$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});	

    var viewport_wid = viewport().width;
    var viewport_height = viewport().height;

    if (viewport_wid <= 991) {

    }
}
$(window).bind('load', handler);
$(window).bind('resize', handler);


/*cursor animation*/
document.addEventListener("DOMContentLoaded", function (event) {
    var cursor = document.querySelector(".custom-cursor");
    var links = document.querySelectorAll("a, button, input.btn, .cursor-item, .box-field");

    var initCursor = false;

    for (var i = 0; i < links.length; i++) {
        var selfLink = links[i];

        selfLink.addEventListener("mouseover", function () {
            cursor.classList.add("custom-cursor_link");
        });
        selfLink.addEventListener("mouseout", function () {
            cursor.classList.remove("custom-cursor_link");
        });
    }


    window.onmousemove = function (e) {
        var mouseX = e.clientX;
        var mouseY = e.clientY;

        if (!initCursor) {
            // cursor.style.opacity = 1;
            TweenLite.to(cursor, 0.3, {
                opacity: 1
            });
            initCursor = true;
        }

        TweenLite.to(cursor, 0, {
            top: mouseY + "px",
            left: mouseX + "px"
        });
    };

    window.ontouchmove = function (e) {
        var mouseX = e.touches[0].clientX;
        var mouseY = e.touches[0].clientY;
        if (!initCursor) {
            // cursor.style.opacity = 1;
            TweenLite.to(cursor, 0.3, {
                opacity: 1
            });
            initCursor = true;
        }

        TweenLite.to(cursor, 0, {
            top: mouseY + "px",
            left: mouseX + "px"
        });
    };

    window.onmouseout = function (e) {
        TweenLite.to(cursor, 0.3, {
            opacity: 0
        });
        initCursor = false;
    };

    window.ontouchstart = function (e) {
        TweenLite.to(cursor, 0.3, {
            opacity: 1
        });
    };

    window.ontouchend = function (e) {
        setTimeout(function () {
            TweenLite.to(cursor, 0.3, {
                opacity: 0
            });
        }, 200);
    };
});

function mainBlockAnimation() {
    var mainOverlays = $('.t--overlays i');
    window.setTimeout(function(){mainOverlays.eq(2).addClass("active");}, 300);
    window.setTimeout(function(){mainOverlays.eq(1).addClass("active");}, 550);
    window.setTimeout(function(){mainOverlays.eq(0).addClass("active");}, 750);
    window.setTimeout(function(){$('.t--overlays').css("display", "none");}, 1000);
    window.setTimeout(function(){$('.main-wrapper').addClass("active");}, 700);
    $("html, body").animate({ scrollTop: 0 }, "slow");
};

$(window).scroll(function () {
	if ($(this).scrollTop() > 350 && $("#fixed-btn").hasClass("fixed_btn")) {
		$("#fixed-btn").removeClass("fixed_btn").addClass("open-btn-fixed");
	} else if ($(this).scrollTop() <= 350 && $("#fixed-btn").hasClass("open-btn-fixed")) {
		$("#fixed-btn").removeClass("open-btn-fixed").addClass("fixed_btn");
	}
});